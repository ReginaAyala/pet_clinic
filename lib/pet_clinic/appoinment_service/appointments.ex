defmodule PetClinic.AppoinmentService.Appointments do
  use Ecto.Schema
  import Ecto.Changeset

  @moduledoc """
  This is module Appointments.
  """
  schema "appointments" do
    field :date_time, :naive_datetime
    belongs_to :pets, PetClinic.PetClinicService.Pet, foreign_key: :pet_id
    belongs_to :health_expert, PetClinic.PetHealthExpert.Vet, foreign_key: :health_expert_id
  end

  def changeset(appointment, attrs \\ %{}) do
    appointment
    |> cast(attrs, [:pet_id, :health_expert_id, :date_time])
    |> validate_required([:pet_id, :health_expert_id, :date_time])
  end
end
