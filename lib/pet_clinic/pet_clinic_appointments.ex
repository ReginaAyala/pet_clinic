defmodule PetClinic.PetHealthExpertAppointments do
  @moduledoc """
  This module is for create appointment and schedule for my unit test
  """
  import Ecto.Query, warn: false
  alias PetClinic.Repo
  alias PetClinic.AppoinmentService.ExpertSchedule
  alias PetClinic.AppoinmentService.Appointments

  def create_schedule(attrs \\ %{}) do
    %ExpertSchedule{}
    |> ExpertSchedule.changeset(attrs)
    |> Repo.insert()
  end

  def create_appointment(attrs \\ %{}) do
    %Appointments{}
    |> Appointments.changeset(attrs)
    |> Repo.insert()
  end
end
