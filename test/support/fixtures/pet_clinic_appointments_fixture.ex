defmodule PetClinic.PetClinicAppointmentsFixtures do
  @moduledoc """
  This is the module to create the fixture for unit text 
  """

  alias PetClinic.PetHealthExpertFixtures
  alias PetClinic.PetClinicServiceFixtures
  alias PetClinic.Repo

  def schedule_fixture(attrs \\ %{}) do
    health_expert = PetHealthExpertFixtures.vet_fixture()

    {:ok, schedule} =
      attrs
      |> Enum.into(%{
        health_expert_id: health_expert.id,
        monday_start: ~T[10:00:00],
        monday_end: ~T[11:00:00]
      })
      |> PetClinic.PetHealthExpertAppointments.create_schedule()

    schedule
  end

  def appointment_fixture(attrs \\ %{}) do
    pet = PetClinicServiceFixtures.pet_fixture()
    health_expert = PetHealthExpertFixtures.vet_fixture()

    {:ok, appointment} =
      attrs
      |> Enum.into(%{
        health_expert_id: health_expert.id,
        pet_id: pet.id,
        date_time: ~N[2022-05-28 12:00:00]
      })
      |> PetClinic.PetHealthExpertAppointments.create_appointment()

    appointment |> Repo.preload(:health_expert)
  end
end
