defmodule PetClinic.PetClinicAppointmentTest do
  use PetClinic.DataCase
  alias PetClinic.AppointmentService.AppointmentService
  alias PetClinic.Repo

  describe "appointments" do
    import PetClinic.PetClinicAppointmentsFixtures
    # alias PetClinic.AppoinmentService.Appointments

    # @invalid_attrs %{health_expert_id: nil}

    test "when to date is less than from date" do
      schedule = schedule_fixture()
      id = schedule.health_expert_id

      assert {:error, "wrong date range"} ==
               AppointmentService.available_slots(
                 id,
                 ~D[2022-05-30],
                 ~D[2022-05-22]
               )
    end

    test "when from date is in the past" do
      schedule = schedule_fixture()
      id = schedule.health_expert_id

      assert {:error, "datetime is in the past"} ==
               AppointmentService.available_slots(
                 id,
                 ~D[2022-05-23],
                 ~D[2022-05-24]
               )
    end

    test "when from date and to date are equal" do
      schedule = schedule_fixture()
      id = schedule.health_expert_id

      assert [%{~D[2022-05-30] => [~T[10:00:00], ~T[10:30:00.000000], ~T[11:00:00.000000]]}] ==
               AppointmentService.available_slots(
                 id,
                 ~D[2022-05-30],
                 ~D[2022-05-30]
               )
    end

    test "when from_date is less than to_date" do
      schedule = schedule_fixture()
      id = schedule.health_expert_id

      assert [%{~D[2022-05-30] => [~T[10:00:00], ~T[10:30:00.000000], ~T[11:00:00.000000]]}] ==
               AppointmentService.available_slots(
                 id,
                 ~D[2022-05-28],
                 ~D[2022-05-30]
               )
    end

    test "no exist expert_id" do
      appointment = appointment_fixture()
      date = appointment.date_time
      pet_id = appointment.pet_id

      assert {:error, "No expert id"} == AppointmentService.new_appointmen(10, pet_id, date)
    end

    test "no exist ped_id" do
      schedule = schedule_fixture()
      appointment = appointment_fixture()
      expert_id = schedule.health_expert_id
      date = appointment.date_time

      assert {:error, "No pet dont exist"} ==
               AppointmentService.new_appointmen(expert_id, 10, date)
    end

    test "all data are good" do
      schedule = schedule_fixture()
      appointment = appointment_fixture()
      expert_id = schedule.health_expert_id
      date = ~N[2022-05-30 10:00:00]
      pet_id = appointment.pet_id

      assert {:ok, "add appointment"} ==
               AppointmentService.new_appointmen(expert_id, pet_id, date)
    end

    test "No hour for this day" do
      schedule = schedule_fixture()
      appointment = appointment_fixture()
      expert_id = schedule.health_expert_id
      date = appointment.date_time
      pet_id = appointment.pet_id

      assert {:error, "NO hour for this day"} ==
               AppointmentService.new_appointmen(expert_id, pet_id, date)
    end

    test "id_expert no exist" do
      appointment = appointment_fixture()
      vet = appointment.health_expert |> Repo.preload(:appoinments)

      assert vet == AppointmentService.filter_appointments(vet, "2022-05-28")
    end

    test "get appointment" do
      appointment = appointment_fixture()
      id_expert = appointment.health_expert_id
      vet = appointment.health_expert |> Repo.preload(appoinments: [pets: [:pet_type, :owner]])

      assert vet == AppointmentService.get_appointment(id_expert, "2022-05-28")
    end
  end
end
